#include <filesystem>
#include <string>

bool is_on_same_filesystem(const std::filesystem::path &a, const std::filesystem::path &b);
bool is_mounted(const std::filesystem::path &path);

void unmount(const std::filesystem::path &path);

void create_union(const std::filesystem::path &local_dir, const std::filesystem::path &system_dir,
                  const std::filesystem::path &mount_point);
