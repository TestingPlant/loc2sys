#include "env.hpp"
#include <cstdlib>
#include <optional>
#include <string>

std::optional<std::string> get_env(const std::string &name) {
	const char *value = std::getenv(name.data());

	return value ? std::optional<std::string>(std::in_place, value) : std::optional<std::string>();
}
