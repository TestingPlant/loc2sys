# loc2sys

loc2sys (local to system) is a program that creates a directory that makes local files appear like system files. It maps `~/.local` to `/usr`,
`~/.config` to `/etc`, and `~/.var` to `/var`.

# Installing programs without root

Installing programs without root is possible with this without any change to the package manager. For example:

```bash
loc2sys -- fakechroot fakeroot pacman -S mesa-utils
```

Afterwards, you can run the program using loc2sys:

```bash
loc2sys -- glxgears
```

or using:

```bash
LD_LIBRARY_PATH=~/.local/lib ~/.local/bin/glxgears
```

Caveats:

- This can't be used for setuid binaries like `sudo`
- This setup is very likely not going to be supported by your distribution
- To avoid a [partial upgrade](https://wiki.archlinux.org/title/System_maintenance#Partial_upgrades_are_unsupported), the system must be updated as root and locally at the same time

# Installing programs temporarily

This tool can be used to install programs temporarily by using a temporary directory for the home.

For example, run:

```bash
HOME=/tmp/temporary_home loc2sys
```

A shell should open. Afterwards, if you run:

```bash
fakechroot fakeroot pacman -S mesa-utils
```

you can run `glxgears` as much as you'd like.

To remove the installed files, simply remove the `/tmp/temporary_home` directory.

# Getting in touch

Feel free to hop on #loc2sys on [OFTC](https://oftc.net/).
