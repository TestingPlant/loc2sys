#include <filesystem>
#include <vector>

class Overlay {
public:
	std::filesystem::path root;

	// Vector of files to bind in proot
	std::vector<std::filesystem::path> binds;

	Overlay();
	~Overlay();
};
