#include <string>
#include <vector>

[[nodiscard]] int run_program(std::string program, std::vector<std::string> arguments);
