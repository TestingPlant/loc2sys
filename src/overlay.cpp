#include "overlay.hpp"
#include "env.hpp"
#include "filesystem.hpp"
#include <cassert>
#include <cstdlib>
#include <filesystem>
#include <iostream>
#include <stdlib.h>
#include <unordered_map>

Overlay::Overlay() {
	const std::optional<std::string> home_string = get_env("HOME");
	if (!home_string) {
		std::cerr << "loc2sys: HOME must be defined\n";
		std::exit(1);
	}

	const std::filesystem::path home = home_string.value();
	const std::filesystem::path system_root = "/";

	std::string root_template = (std::filesystem::temp_directory_path() / "loc2sys.XXXXXX").string();
	const char *root_string = mkdtemp(root_template.data());
	assert(root_string);
	root = root_string;

	// Make unions to the user's directories
	create_union(home / ".local", system_root / "usr", root / "usr");
	create_union(home / ".config", system_root / "etc", root / "etc");
	create_union(home / ".var", system_root / "var", root / "var");

	// Add bindings for the rest of the directories
	for (const std::filesystem::directory_entry &system_root_file :
	     std::filesystem::directory_iterator(system_root)) {
		const std::string filename = system_root_file.path().filename().string();
		const std::filesystem::path root_file = root / filename;

		if (std::filesystem::exists(root_file))
			continue;

		binds.push_back(system_root_file.path());
	}
}

Overlay::~Overlay() {
	std::filesystem::remove_all(root / ".unionfs");

	for (const auto &union_filesystem : std::filesystem::directory_iterator(root)) {
		unmount(union_filesystem.path());
		std::filesystem::remove(union_filesystem.path());
	}

	std::filesystem::remove(root);
}
