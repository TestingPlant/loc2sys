#include "filesystem.hpp"
#include "overlay.hpp"
#include "program.hpp"
#include <cstdlib>
#include <cxxopts.hpp>
#include <filesystem>
#include <fmt/format.h>
#include <system_error>

int main(int argc, char **argv) {
	cxxopts::Options options("loc2sys", "Run a program with local files mapped to system files");

	cxxopts::OptionAdder add_option = options.add_options();
	add_option("h,help", "Show this help message");

	const auto parsedOptions = options.parse(argc, argv);

	if (parsedOptions.count("help")) {
		std::cout << options.help() << "\n";
		return 0;
	}

	std::vector<std::string> arguments = parsedOptions.unmatched();
	if (arguments.empty()) {
		const char *shell = std::getenv("SHELL");
		if (!shell)
			shell = "sh";

		arguments = {shell};
	}

	Overlay overlay;

	std::vector<std::string> argumentsToInsert;
	argumentsToInsert.reserve(2 + 2 * overlay.binds.size());
	argumentsToInsert.emplace_back("-r");
	argumentsToInsert.emplace_back(overlay.root.string());

	for (const std::filesystem::path &bind : overlay.binds) {
		argumentsToInsert.emplace_back("-b");
		argumentsToInsert.emplace_back(bind.string());
	}

	arguments.insert(arguments.begin(), argumentsToInsert.begin(), argumentsToInsert.end());

	return run_program("proot", arguments);
}
