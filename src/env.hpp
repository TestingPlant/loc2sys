#include <optional>
#include <string>

std::optional<std::string> get_env(const std::string &name);
