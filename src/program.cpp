#include <cerrno>
#include <string>
#include <sys/wait.h>
#include <system_error>
#include <unistd.h>
#include <vector>

[[nodiscard]] int run_program(std::string program, std::vector<std::string> arguments) {
	pid_t child_pid = fork();

	if (child_pid == -1) {
		throw std::system_error(errno, std::generic_category(), "fork() failed");
	} else if (child_pid == 0) {
		// This is the child process

		// Create a vector of const char*s to pass to execvp

		std::vector<char *> argumentPointers;
		argumentPointers.reserve(arguments.size() + 2);
		argumentPointers.push_back(program.data());
		for (std::string &argument : arguments)
			argumentPointers.push_back(argument.data());
		argumentPointers.push_back(nullptr);

		// Run the program
		execvp(program.data(), argumentPointers.data());

		// If this code is still running, an error occured
		throw std::system_error(errno, std::generic_category(), "execvp() failed");
	} else {
		// This is the parent process. It waits for the child process to exit to get its exit code.
		int child_status;
		if (waitpid(child_pid, &child_status, 0) == -1)
			throw std::system_error(errno, std::generic_category(), "waitpid() failed");
		return WEXITSTATUS(child_status);
	}
}
