#include "filesystem.hpp"
#include "program.hpp"
#include <cstdlib>
#include <filesystem>
#include <fmt/format.h>

void unmount(const std::filesystem::path &path) {
	int exit_code = run_program("umount", {path.string()});

	if (exit_code != 0) {
		fmt::print(stderr, "Failed to create overlay: umount exited with code {}\n", exit_code);
		std::exit(exit_code);
	}
}

void create_union(const std::filesystem::path &local_dir, const std::filesystem::path &system_dir,
                  const std::filesystem::path &mount_point) {
	std::filesystem::create_directories(local_dir);
	std::filesystem::create_directories(mount_point);

	const int exit_code = run_program(
	    "unionfs", {"-o", "auto_unmount", "-o", "cow", "-o", "hide_meta_files", "-o", "relaxed_permissions",
	                fmt::format("{}=RW:{}=RO", local_dir.string(), system_dir.string()), mount_point.string()});

	if (exit_code != 0) {
		fmt::print(stderr, "Failed to create overlay: unionfs exited with code {}\n", exit_code);
		std::exit(exit_code);
	}
}
